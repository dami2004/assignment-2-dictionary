%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

section .rodata
%include "word.inc"
start_msg:
    db 'Enter the key: ', 0
err_msg: 
    db 'Couldn not read the word', 10, 0
not_found_msg: 
    db 'The key was not found in the dictionary', 10, 0
found_msg: 
    db 'Value by key: ', 0

section .text
extern find_word
extern print_dict
%include "lib.inc"

global _start
_start:

    mov rdi, start_msg
    mov rsi, STDOUT    
    call print_string

    sub rsp, BUFFER_SIZE 
    mov rsi, BUFFER_SIZE 
    mov rdi, rsp
    call read_word
    test rax, rax 
    jz .err_read
    mov rsi, first
    mov rdi, rax
    call find_word 
    test rax, rax  

    je .not_found

    .found:
        add rax, OFFSET 
        push rax
        mov rsi, STDOUT
        mov rdi, found_msg
        call print_string 

        
        pop rax 
        mov rdi, rax
        call string_length
        inc rax
        add rdi, rax 
        mov rsi, STDOUT
        call print_string 
        jmp .end

    .err_read:
        mov rdi, err_msg
        mov rsi, STDERR 
        call print_string
        jmp .end

    .not_found:
        mov rdi, not_found_msg
        mov rsi, STDERR 
        call print_string
        

    .end:
        add rsp, BUFFER_SIZE 
        call exit
	
