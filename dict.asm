section .text
%include "lib.inc"
%define eight 8
global find_word
global print_dict


find_word:
    .loop:
    	push rsi
    	add rsi, eight	
    	call string_equals 
    	pop rsi
    	test rax, rax
    	jne .found 
    	mov rsi, [rsi] 	
    	cmp rsi, 0 	
    	je .not_found 
    	jmp .loop 
    .found:
        mov rax, rsi 
        ret
    .not_found:
        xor rax, rax
        ret	

       
print_dict:

    .loop:
        push rsi            
        push rdi            
        add rdi, eight          
        call print_string  
        call print_newline
        pop rdi            
        pop rsi            
        mov rdi, [rdi]     
        test rdi, rdi
        jnz .loop           
        xor rax, rax        
    ret   
