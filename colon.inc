%define first 0
;macros



%macro colon 2
    %ifid %2
    %else
        %error "Invalid mark name"
    %endif

    %ifstr %1
    %else
        %error "Invalid key name"
    %endif

    %2:
    dq first
    db %1, 0
    
%define next %2                       
%endmacro
